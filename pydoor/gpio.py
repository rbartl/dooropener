import RPi.GPIO as GPIO
import time
 
# Use GPIO numbers not pin numbers
GPIO.setmode(GPIO.BCM)
 
# set up the GPIO channels - one input and one output
GPIO.setup(25, GPIO.OUT)
 
## output to GPIO8

GPIO.output(25, True)
time.sleep (1)
GPIO.output(25, False)

GPIO.cleanup()