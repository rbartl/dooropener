#!/usr/bin/python

import socket
import RPi.GPIO as GPIO
import time
import httplib, urllib
import syslog


syslog.syslog('udpserv started')

UDP_IP = "127.0.0.1"
UDP_PORT = 9876

sock = socket.socket(socket.AF_INET, # Internet
                     socket.SOCK_DGRAM) # UDP
sock.bind((UDP_IP, UDP_PORT))

while True:
    data, addr = sock.recvfrom(1024) # buffer size is 1024 bytes
    print "received message:", data
    syslog.syslog("udpserv: received message. " + data)
    if data == "{len=7,id='04:40:1e:1a:3f:34:80:00:00:00'}" or data == "{len=7,id='04:2b:4c:3a:58:3b:80:00:00:00'}" or data == "{len=7,id='04:70:4a:3a:58:3b:80:00:00:00'}" or data == "{len=7,id='04:34:4d:3a:58:3b:80:00:00:00'}" or data == "{len=7,id='04:30:1f:a2:de:32:80:00:00:00'}" or data == "{len=7,id='04:4e:1f:a2:de:32:80:00:00:00'}" or data == "{len=7,id='04:5d:20:a2:de:32:80:00:00:00'}" or data == "{len=7,id='04:3f:49:3a:58:3b:80:00:00:00'}" or data == "{len=7,id='04:6f:1f:1a:3f:34:80:00:00:00'}":
      syslog.syslog("opening door")
      # Use GPIO numbers not pin numbers
      GPIO.setmode(GPIO.BCM)
      
      # set up the GPIO channels - one input and one output
      GPIO.setup(25, GPIO.OUT)
      
      ## output to GPIO8
      
      GPIO.output(25, True)
      time.sleep (1)
      GPIO.output(25, False)
      
      GPIO.cleanup()

      conn = httplib.HTTPSConnection("api.pushover.net:443")
      conn.request("POST", "/1/messages.json",
      urllib.urlencode({
          "token": "a9Yx6HitnwG7JKTsgFDJXiG8rkX98Z",
          "user": "ueu2sLT5obfQhD2XHMEiE7HhTie9Mz",
          "message": "door used by " + data,
        }), { "Content-type": "application/x-www-form-urlencoded" })
      conn.getresponse()
                
                    